<div class="row d-block searchform-wrapper">

    <div class="col-12">
        <form class="form-inline form-blog" id="searchform" method="get" action="<?php echo home_url('/'); ?>">

                <div class="form-group input-group w-100">
                    <input name="s" class="form-control" type="text" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>" id="pesquisa" />
                    <button type="submit"><img class="submit-search" src="<?php echo get_template_directory_uri() . '/assets/img/submit-search.png'; ?>" alt="">
                    </button>
                </div>

        </form>
    </div>

</div>