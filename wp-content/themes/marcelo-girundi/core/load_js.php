<?php

function pbo_load_js()
{       
    # wp_enqueue_script('loadingJS', get_template_directory_uri() . '/core/components/loading/loading.js', 1, false);  

    
    # Custom JS
    wp_enqueue_script('Bootstrap', get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.min.js', array('jquery'), 1, true);
    wp_enqueue_script('jQuery-Easing', get_template_directory_uri() . '/node_modules/jquery.easing/jquery.easing.js', array('jquery'), 1, true);
    wp_enqueue_script('Waypoint', get_template_directory_uri() . '/node_modules/waypoints/lib/jquery.waypoints.min.js', array('jquery'), 1, true);
    wp_enqueue_script('FontAwesome', get_template_directory_uri() . '/node_modules/@fortawesome/fontawesome-free/js/all.min.js', array('jquery'), 1, true);   
    wp_enqueue_script('owlCarousel-Js', get_template_directory_uri() . '/node_modules/owl.carousel/dist/owl.carousel.min.js', array('jquery'), 1, true);     
    wp_enqueue_script('WOW-Js', get_template_directory_uri() . '/node_modules/wowjs/dist/wow.min.js', array('jquery'), 1, true);   
    wp_enqueue_script('MakedInput-Js', get_template_directory_uri() . '/node_modules/jquery.maskedinput/src/jquery.maskedinput.js', array('jquery'), 1, true);
    wp_enqueue_script('Global-Js', get_template_directory_uri() . '/assets/js/global.js', array('jquery'), 1, true);
    wp_enqueue_script('select2', get_template_directory_uri() . '/node_modules/select2/dist/js/select2.min.js', array('jquery'), 1, true);
    wp_enqueue_script('fancyBox', get_template_directory_uri() . '/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js', array('jquery'), 1, true);



    wp_enqueue_script('utilsCore', get_template_directory_uri() . '/core/assets/js/utilsFramework.js', array('jquery'), 1, true);     
}

add_action('wp_enqueue_scripts', 'pbo_load_js');
