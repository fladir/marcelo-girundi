<?php
$args = array(
    'post_type' => 'depoimento',
    'order' => 'ASC',
    'posts_per_page' => 50,
);
$WPQuery = new WP_Query($args);
?>

<section class="d-flex align-items-center" id="depoimentos" style="background-image: url(<?php print_r(get_field('imagem_de_fundo_depoimentos', 128)['sizes']['fundo_secao']) ?>)">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-5 pb-5">
                <h3 class="text-uppercase fw-semi-bold text-white text-center">Depoimentos</h3>
            </div>
        </div>
        <div class="owl-carousel owl-theme depoimentos-carousel animated fadeIn">
            <?php while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                <div class="item">
                    <div class="content align-self-stretch flex-column">

                        <span class="baloon">
                            <?php the_content(); ?>
                        </span>
                        <h5 class="mt-4 text-white"><?php the_title() ?></h5>
                        <p class="text-white"><?php echo get_field('subtitulo_depoimentos'); ?></p>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endwhile; ?>
        </div>
    </div>

</section>