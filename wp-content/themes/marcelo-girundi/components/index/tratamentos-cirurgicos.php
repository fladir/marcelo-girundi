<?php
$oQueEObesidade = get_field('grupo_o_que_e_obesidade');

$args = array(
    'post_type' => 'tratamento_cirurgico',
    'order' => 'ASC',
    'posts_per_page' => 50,
);
$WPQuery = new WP_Query($args);

?>

<section id="tratamentos-cirurgicos-home">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-5">
                <h3 class="text-uppercase fw-semi-bold text-primario text-center">Tratamentos Cirúrgicos</h3>
            </div>
        </div>
        <div class="owl-carousel owl-theme tratamentos-cirurgicos-carousel">
            <?php while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                <div class="item">
                    <a href="<?php the_permalink(); ?>" class="img-link">
                        <?php the_post_thumbnail('full_posts', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                        <span class="overlay d-flex justify-content-center align-items-center">
                            <i class="fas fa-plus"></i>
                        </span>
                    </a>
                    <div class="content align-self-stretch flex-column">
                        <a href="<?php the_permalink(); ?>" class="img-link">
                            <h5 class="fw-semi-bold text-primario mt-4 mb-3"><?php the_title() ?></h5>
                        </a>
                        <p class="mb-4">
                            <?php echo word_count(get_the_excerpt(), '15'); ?>...
                        </p>
                        <a href="<?php the_permalink(); ?>" class="btn btn-primario btn-small">Saiba Mais</a>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endwhile; ?>
        </div>
    </div>
</section>