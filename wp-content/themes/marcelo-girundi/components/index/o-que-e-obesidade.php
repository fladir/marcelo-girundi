<?php
$oQueEObesidade = get_field('grupo_o_que_e_obesidade');
#echo '<pre>'; print_r($oQueEObesidade); echo '</pre>';
?>

<section id="o-que-e-obesidade">
    <div class="container container-large">
        <div class="row">
            <div class="col-md-5">
                <img src="<?php print_r($oQueEObesidade['imagem']['sizes']['img_secoes_alternadas']) ?>"
                     alt="<?php echo $oQueEObesidade['titulo'] ?>" title="<?php echo $oQueEObesidade['titulo'] ?>"
                     class="secoes-alternadas">

            </div>
            <div class="col-md-7">
                <h2 class="text-uppercase mb-4"><?php echo $oQueEObesidade['titulo'] ?></h2>

                <?php echo $oQueEObesidade['texto'] ?>

                <a href="<?php echo $oQueEObesidade['link_do_botao']; ?>"
                   class="btn btn-secundario mt-4">
                    <?php echo $oQueEObesidade['texto_do_botao']; ?>
                </a>

            </div>
        </div>
    </div>
</section>