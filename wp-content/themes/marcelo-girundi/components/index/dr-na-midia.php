<?php
$drNaMidia = get_field('dr_na_midia', 128);
#echo '<pre>'; print_r($drNaMidia); echo '</pre>';
?>

<section id="dr-na-midia">
    <div class="container container-large">
        <div class="row">

            <div class="col-md-7">
                <h2 class="text-uppercase mb-4"><?php echo $drNaMidia['titulo'] ?></h2>

                <?php echo $drNaMidia['texto'] ?>

                <a href="<?php echo $drNaMidia['link_do_botao']; ?>"
                   class="btn btn-secundario mt-4">
                    <?php echo $drNaMidia['texto_do_botao']; ?>
                </a>

            </div>

            <div class="col-md-5">
                <img src="<?php print_r($drNaMidia['imagem']['sizes']['img_secoes_alternadas']) ?>"
                     alt="<?php echo $drNaMidia['titulo'] ?>" title="<?php echo $drNaMidia['titulo'] ?>"
                     class="secoes-alternadas">

            </div>
        </div>
    </div>
</section>