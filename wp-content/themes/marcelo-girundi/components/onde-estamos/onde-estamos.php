<?php
$conteudoDosComponentes = get_field('grupo_conteudos_dos_componentes', 'options');
$ondeEstamos = $conteudoDosComponentes['onde_estamos'];
?>

<section id="onde-estamos">
    <div class="container">
        <div class="row wow fadeIn">
            <div class="col-8 col-md-6">
                <div class="conteudo">
                    <h2><?php echo $ondeEstamos['titulo'] ?></h2>
                    <p class="text-white">
                        <?php echo $ondeEstamos['texto'] ?>
                    </p>
                    <a href="<?php echo $ondeEstamos['link_do_botao'] ?>" class="btn btn-black mt-3"><?php echo $ondeEstamos['texto_do_botao'] ?></a>
                </div>
            </div>
            <div class="col-4 col-md-6">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/mockup-mobile.png'; ?>"
                     alt="<?php echo $ondeEstamos['titulo'] ?>" class="mockoup-mobile">
            </div>
        </div>
    </div>
</section>
