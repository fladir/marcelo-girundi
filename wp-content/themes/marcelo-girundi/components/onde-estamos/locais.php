<?php
$cidades = get_field('cidade');
$locais = $cidades['locais'];
#echo '<pre>'; print_r($cidades); echo '</pre>';
?>

<div class="row locais">
    <?php if ($cidades) : foreach ($cidades as $cidade) :
        $locais = $cidade['locais'];

        #echo '<pre>'; print_r($locais); echo '</pre>';

        ?>
        <div class="col-md-4 mb-4">
            <div class="content d-flex align-items-start">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/local.png'; ?>"
                     alt="<?php echo $cidade['nome_da_cidade'] ?>" class="marker-local mr-2">

                <div class="content-inner">
                    <h4 class="fw-bold text-primario">
                        <?php echo $cidade['nome_da_cidade'] ?>
                    </h4>

                    <?php
                    $i = 0;

                    if ($locais) : foreach ($locais as $local) :
                        $i == 0 ? $class = '' : $class = 'bordered';
                        ?>
                        <div class="content-locais d-flex flex-column <?php echo $class; ?>">
                            <?php if ($local['nome_do_local']) : ?>
                                <p class="fw-bold"><?php echo $local['nome_do_local'] ?></p>
                            <?php endif; ?>
                            <p><?php echo $local['endereco'] ?></p>
                            <p>Telefone(s): <?php echo $local['telefones'] ?></p>
                        </div>
                        <?php
                        $i++;
                    endforeach; endif;
                    ?>
                </div>
            </div>
        </div>
    <?php endforeach; endif; ?>
</div>
