<?php
$items = get_field('items_institucional')
?>

<section id="institucional">
    <div class="container">
        <div class="row">
            <?php if($items) : foreach ($items as $item) : ?>
            <div class="col-md">
                <h4 class="text-white text-uppercase text-center fw-bold mb-4"><?php echo $item['titulo']; ?></h4>
                <img src="<?php print_r($item['icone']['sizes']['icone_institucional']) ?>"
                     alt="<?php echo $drNaMidia['titulo'] ?>" title="<?php echo $drNaMidia['titulo'] ?>"
                     class="icone-institucional mb-5">
                <p class="text-white px-4 text-justify"><?php echo $item['texto'] ?></p>
            </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>
