<?php
$titulo = get_field('titulo_da_secao');
$nossosValores = get_field('nossos_valores');
?>

<section id="nossos-valores">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="titulo mb-5">
                    <?php echo $titulo ?>
                </h2>
            </div>
            <?php if ($nossosValores) : foreach ($nossosValores as $nossoValor) : ?>
                <div class="col-md-4 mb-4">
                    <div class="conteudo">
                        <img src="<?php print_r($nossoValor['icone']['sizes']['nossos_valores']) ?>"
                             alt="<?php echo $nossoValor['titulo'] ?>" title="<?php echo $nossoValor['titulo'] ?>"
                             class="icon mb-5">
                        <h3 class="text-center fw-bold mb-2">
                            <?php echo $nossoValor['titulo'] ?>
                        </h3>
                        <p class="text-center">
                            <?php echo $nossoValor['texto'] ?>
                        </p>
                    </div>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>
