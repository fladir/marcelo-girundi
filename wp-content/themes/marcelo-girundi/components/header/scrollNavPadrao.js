(function ($) {
    $(document).ready(function () {
        $(window).resize(function () {
            var lastScrollTop = 0;
            if ($(window).width() > 999) {
                $(window).scroll(function (event) {
                    var st = $(this).scrollTop();
                    if (st > lastScrollTop && st > 166) {
                        $('#navbar').addClass('animated slideOutUp');
                    } else if (st < 166) {
                        $('#navbar').removeClass('slideOutUp fixed-top menu_sticky')
                    } else {
                        $('#navbar').removeClass('slideOutUp').addClass('slideInDown fixed-top menu_sticky')
                    }
                    lastScrollTop = st;
                });

            } else {
                $(window).scroll(function (event) {
                    var st = $(this).scrollTop();
                    if (st > lastScrollTop) {
                        $('#navbar').addClass('animated slideOutUp')
                    }else if(st < 10){
                        $('#navbar').removeClass('menu_sticky')
                    } else {
                        $('#navbar').removeClass('slideOutUp').addClass('slideInDown fixed-top menu_sticky')
                    }
                    lastScrollTop = st;

                });
            }
        }).resize();
    });
})(jQuery);