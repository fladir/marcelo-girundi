<?php
$botaoPrimario = get_fields('options')['grupo_header']['botao_primario'];
$botaoSecundario = get_fields('options')['grupo_header']['botao_secundario']
?>
<?php #echo'<pre>'; print_r($botaoPrimario); echo'</pre>'; ?>
<div id="navbarWapper">
    <nav id="menu-secundario" role="navigation">

        <div class="container">
<div class="row">
    <div class="col-md-3">
            <a class="navbar-brand" href="<?php bloginfo('url'); ?>" title="">
                <?php echo wp_get_attachment_image(get_field('grupo_header', 'options')['logo_colorida'], 'logo'); ?>
            </a>
    </div>

            <?php get_template_part('components/header/menu_primario'); ?>

</div>
            <?php if (is_home() || is_front_page()) : ?>
                <h1 class="sr-only"><?php echo get_field('grupo_header', 'options')['nome_da_empresa'] ? get_field('grupo_header', 'options')['nome_da_empresa'] : get_bloginfo('name'); ?></h1>
            <?php endif; ?>


            <?php if ($botaoPrimario['texto']) : ?>
                <a style="font-size: 13px;" href="<?php echo $botaoPrimario['link'] ?>"
                   class="btn btn-primario d-none d-lg-block ml-3"><?php echo $botaoPrimario['texto'] ?></a>
            <?php endif; ?>
            <?php if ($botaoSecundario['texto']) : ?>
                <a style="font-size: 13px;" href="<?php echo $botaoSecundario['link'] ?>"
                   class="btn btn-secundario d-none d-lg-block ml-4"><?php echo $botaoSecundario['texto'] ?></a>
            <?php endif; ?>

            <div id="navTogglerFixed" class="nav-toggler d-sm-block d-lg-none">
                <span></span>
            </div>
        </div>
        <div class="nav-toggler d-sm-block d-lg-none">
            <span></span>
        </div>
    </nav>
    <nav id="navbar" class="navbar navbar-expand-lg">
        <div class="container position-relative">
            <?php

            wp_nav_menu(array(
                'theme_location' => 'primary',
                'depth' => 3,
                'container' => 'div',
                'container_class' => 'collapse navbar-collapse nav',
                'container_id' => 'navbarNav',
                'menu_class' => 'nav navbar-nav w-100 justify-content-center',
                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                'walker' => new WP_Bootstrap_Navwalker(),
            ));


            ?>
        </div>
    </nav>
</div>
<?php wp_enqueue_script('headerNavPadraoJS', get_template_directory_uri() . '/components/header/scrollNavPadrao.js', array('jquery'), 1, true); ?>
<?php get_template_part('components/header/mobile_menu'); ?>

</div>
<div id="navTogglerFixed" class="nav-toggler d-sm-block d-lg-none">
    <span></span>
</div>




