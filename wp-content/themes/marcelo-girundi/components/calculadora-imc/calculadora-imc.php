<?php

?>

<section id="calculadora-imc">
    <div class="container container-large">
        <div class="row">
            <div class="col-md-5">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/img-calc-imc.jpg'; ?>"
                     alt="<?php the_title() ?>" class="secoes-alternadas">

            </div>
            <div class="col-md-7 d-flex justify-content-center flex-column">

                <div class="row">
                    <div class="col-md-6 pr-5">
                        <div class="col-12">
                            <h4 class="text-uppercase fw-bold mb-4">Calcule seu IMC </h4>
                        </div>
                        <form action="#" id="calculo">

                            <div class="col-12">
                                <input class="input-imc" type="text" placeholder="Digite seu peso em quilos"
                                       name='peso' id="peso">
                            </div>
                            <div class="col-12">
                                <input class="input-imc" type="text" placeholder="Digite sua altura em metros"
                                       name='altura' id="altura">
                            </div>
                            <div class="col-12">
                                <a href="#" class="calcular btn btn-secundario w-100" rel="nofollow" style="margin-top: 2px;">Calcular IMC</a>
                            </div>

                        </form>


                    </div>
                    <div class="col-md-6">
                        <div class="imc">
                            <div id="resultado" class="resultado-imc">
                                <h4 class="text-uppercase fw-bold mb-4">Resultado:</h4>
                                    <span class="ruslt-text text-white"></span>
                                <h4 class="mb-3 mt-4">Podemos te ajudar?</h4>
                                <a href="#" class="btn btn-destaque">
                                    Agendar Consulta
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
