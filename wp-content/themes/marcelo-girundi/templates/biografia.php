<?php
/* Template Name: Biografia */
get_header();

?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>
    <section id="biografia">
        <div class="container">
            <?php the_content(); ?>
        </div>
    </section>

    <!-- Institucional -->
<?php get_template_part('components/biografia/institucional'); ?>

    <!-- Galeria -->
<?php get_template_part('components/biografia/galeria'); ?>

    <!-- Dr. na Mídia -->
<?php get_template_part('components/index/dr-na-midia'); ?>

<?php get_footer(); ?>