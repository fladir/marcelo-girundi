<?php
/* Template Name: Dúvidas Comuns */
get_header();
$args = array(
    'post_type' => 'faq',
    'order' => 'ASC',
    'posts_per_page' => 50,
);
$WPQuery = new WP_Query($args);
?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>
    <section id="pagina">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4 class="text-primario text-uppercase fw-bold mb-5 pb-3">Veja as dúvidas mais frequentes em nossos pacientes</h4>
                    <div id="accordion" class="toggle-duvidas">
                        <?php while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                            <div class="card">
                                <div class="card-header" id="h-<?php the_ID(); ?>">
                                    <h5 class="mb-0">
                                        <a href="#" class="d-flex align-items-center" data-toggle="collapse"
                                           data-target="#id-<?php the_ID(); ?>" aria-expanded="true"
                                           aria-controls="id-<?php the_ID(); ?>">
                                            <div class="icon-wrapper mr-3">
                                                <i class="fas fa-chevron-right"></i>
                                            </div>
                                            <?php the_title(); ?>
                                        </a>
                                    </h5>
                                </div>

                                <div id="id-<?php the_ID(); ?>" class="collapse" aria-labelledby="h-<?php the_ID(); ?>"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile;
                        wp_reset_query(); ?>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <!-- Dr. na Mídia -->
<?php get_template_part('components/index/dr-na-midia'); ?>

<?php get_footer(); ?>