<?php
get_header();
$dicas = get_field('dicas_de_utilizacao');
$arquivos = $dicas['arquivos'];
?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- Post -->
    <section id="single-produto">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-9 content pr-0 pr-md-5">
                        <h2 class="titulo fw-bold w-50"><?php echo get_field('frase_destaque') ?></h2>
                        <?php if (has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('col_6', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                        <?php endif; ?>
                        <?php the_content(); ?>
                        <p>&nbsp;</p>
                        <h3><?php echo $dicas['titulo'] ?></h3>
                        <p><?php echo $dicas['texto'] ?></p>
                        <?php if($arquivos) : foreach ($arquivos as $arquivo) : ?>
                            <a href="<?php print_r($arquivo['arquivo']['url']) ?>" target="_blank" class="btn btn-secundario download-arquivo">Download<i class="far fa-file-pdf"></i></i></a><p>&nbsp;</p>
                        <?php endforeach; endif; ?>

                    </div>
                    <div class="col-md-3">


                        <?php
                        $terms = get_the_terms($post->ID, 'categoria-produto');
                        foreach ($terms as $term) {
                            $termID[] = $term->slug;
                        }
                        if ($termID[0] == "concreto"):?>

                            <?php
                            wp_nav_menu(array(
                                'menu' => 'Produtos (Concreto)',
                                'theme_location' => 'produtos',
                                'depth' => 3,
                                'container' => 'div',
                                'container_id' => 'menu-categoria-produtos',
                                'container_class' => 'navbar-produtos',
                                'menu_id' => 'navbarCategorias',
                                'menu_class' => 'nav navbar-nav w-100 ',
                                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                                'walker' => new WP_Bootstrap_Navwalker(),
                            ));
                            ?>

                        <?php elseif ($termID[0] == "argamassa") : ?>
                            <?php
                            wp_nav_menu(array(
                                'menu' => 'Produtos (Argamassa)',
                                'theme_location' => 'produtos',
                                'depth' => 3,
                                'container' => 'div',
                                'container_id' => 'menu-categoria-produtos',
                                'container_class' => 'navbar-produtos',
                                'menu_id' => 'navbarCategorias',
                                'menu_class' => 'nav navbar-nav w-100 ',
                                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                                'walker' => new WP_Bootstrap_Navwalker(),
                            ));
                            ?>

                        <?php endif; ?>

                        <?php dynamic_sidebar('sidebar_produtos') ?>

                    </div>

                </div>
            </div>
        </article>
    </section>

    <!-- Onde Estamos -->
<?php get_template_part('components/onde-estamos/onde-estamos'); ?>

    <!-- Call to Action -->
<?php get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>