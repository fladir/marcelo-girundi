<?php
get_header();
$galeria = get_field('galeria_de_fotos');
#echo '<pre>'; print_r($galeria); echo '</pre>'
?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- Post -->
    <section id="single-produto">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-12 content pr-0 pr-md-5">
                        <h2 class="titulo fw-bold w-50"><?php the_title() ?></h2>
                    </div>
                    <div class="col-md-8 offset-md-2 my-4 pt-3">
                        <?php if (has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('nossas_obras_single', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 content pr-0 pr-md-5">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section id="galeria-obras" class="mb-5 pb-5">
        <div class="container">
            <div class="row">
                <?php if ($galeria) : foreach ($galeria as $item) : ?>
                    <div class="col-6 col-md-3 mb-4">
                        <a data-fancybox="gallery" class="position-relative d-block" href="<?php print_r($item['sizes']['full_obras']) ?>">
                            <img src="<?php print_r($item['sizes']['thumb_obras']) ?>">
                            <span class="overlay"><i class="fas fa-search-plus"></i></span>
                        </a>
                    </div>
                <?php endforeach; endif; ?>
            </div>
        </div>
    </section>

    <!-- Onde Estamos -->
<?php get_template_part('components/onde-estamos/onde-estamos'); ?>

    <!-- Call to Action -->
<?php get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>