<?php
add_action('init', 'type_post_tratamentos_cirurgicos');

function type_post_tratamentos_cirurgicos()
{
    $labels = array(
        'name' => _x('Tratamentos Cirúrgicos', 'post type general name'),
        'singular_name' => _x('Tratamento Cirúrgico', 'post type singular name'),
        'add_new' => _x('Adicionar Novo Tratamento', 'Novo Tratamento'),
        'add_new_item' => __('Novo Tratamento'),
        'edit_item' => __('Editar Tratamento'),
        'new_item' => __('Novo Tratamento'),
        'view_item' => __('Ver Tratamento'),
        'search_items' => __('Procurar Tratamentos Cirúrgicos'),
        'not_found' => __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Tratamentos Cirúrgicos'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-heart',
        'show_in_rest' => true,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    );

    register_post_type('tratamento_cirurgico', $args);
    flush_rewrite_rules();
}

add_action('init', 'type_post_depoimentos');

function type_post_depoimentos()
{
    $labels = array(
        'name' => _x('Depoimentos', 'post type general name'),
        'singular_name' => _x('Depoimento', 'post type singular name'),
        'add_new' => _x('Adicionar Novo Depoimento', 'Novo Depoimento'),
        'add_new_item' => __('Novo Depoimento'),
        'edit_item' => __('Editar Depoimento'),
        'new_item' => __('Novo Depoimento'),
        'view_item' => __('Ver Depoimento'),
        'search_items' => __('Procurar Depoimentos'),
        'not_found' => __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Depoimentos'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-testimonial',
        'supports' => array('title', 'editor'),
    );

    register_post_type('depoimento', $args);
    flush_rewrite_rules();
}
add_action('init', 'type_post_duvidas');

function type_post_duvidas()
{
    $labels = array(
        'name' => _x('Dúvidas Comuns', 'post type general name'),
        'singular_name' => _x('Dúvida', 'post type singular name'),
        'add_new' => _x('Adicionar Nova Dúvida', 'Nova Dúvida'),
        'add_new_item' => __('Nova Dúvida'),
        'edit_item' => __('Editar Dúvida'),
        'new_item' => __('Nova Dúvida'),
        'view_item' => __('Ver Dúvida'),
        'search_items' => __('Procurar Dúvidas'),
        'not_found' => __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Dúvidas Comuns'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-editor-help',
        'supports' => array('title', 'editor'),
    );

    register_post_type('faq', $args);
    flush_rewrite_rules();
}

?>