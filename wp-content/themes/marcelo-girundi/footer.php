<?php
$grupoFooter = get_fields('options')['grupo_footer'];
$certificados = $grupoFooter['certificados'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
$whatsapps = get_field('grupo_informacoes_para_contato', 'options')['whatsapp'];
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];

?>

<!-- Footer -->
<footer class="font-small text-white">

    <!-- Footer Links -->
    <div class="container text-center text-sm-left wrapper-footer">

        <!-- Grid row -->
        <div class="row ">

            <div class="col-md-2 logo-footer">
                <a class="logo-footer mb-3" href="<?php bloginfo('url'); ?>">
                    <?php echo wp_get_attachment_image(get_field('grupo_header', 'options')['logo_colorida'], 'logo'); ?>
                </a>

                <?php foreach ($redesSociais as $redesSocial) : ?>
                    <span class="redes-sociais mr-1">
                                    <a href="mailto:<?php echo $redesSocial['link_social']; ?>" target="_blank"
                                       title="<?php echo $redesSocial['nome_rede_social']; ?>">
                                    <i class="<?php echo $redesSocial['icone_social']; ?>"></i>
                                    </a>
                                </span>
                <?php endforeach; ?>

            </div>

            <div class="col-md-5 certificados mt-4 mt-md-0 d-flex justify-content-start align-items-start">

                <?php if ($certificados) : foreach ($certificados as $certificado) : ?>
                    <img src="<?php print_r($certificado['imagem']['sizes']['img_post_list']) ?>"
                         alt="Certificado" title="Certificado"
                         class="cert-footer ml-4">

                <?php endforeach; endif; ?>

            </div>

            <div class="col-md-5 informacoes mt-4 mt-md-0">
                <h4 class="">Agende sua consulta</h4>

                <p class="texto-footer">
                    <?php echo $grupoFooter['Texto_footer'] ?>
                </p>


                    <span class="telefone mr-2 mb-2">
                        <span class="icon-wrapper">
                    <i class="fas fa-phone-alt mr-2 "></i>
                        </span>
                    <?php foreach ($telefones as $telefone) : ?>
                    <a class="mr-2" href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                    <?php echo $telefone['numero_telefone']; ?>
                    </a>
                    <?php endforeach; ?>
                </span>


                <?php foreach ($whatsapps as $whatsapp) : ?>
                    <span class="whatsapp mr-2 mb-2">
                        <span class="icon-wrapper">
                    <i class="fab fa-whatsapp mr-2 "></i>
                        </span>
                    <a href="https://api.whatsapp.com/send?phone=55<?php echo $whatsapp['link_whatsapp']; ?>&text=Ola,%20tudo%20bem?"
                       target="_blank">
                    <?php echo $whatsapp['numero_whatsapp']; ?>
                    </a>
                </span>
                <?php endforeach; ?>

                <?php foreach ($emails as $email) : ?>
                    <span class="email mr-2 mb-2">
                        <span class="icon-wrapper">
                    <i class="fas fa-envelope mr-2 "></i>
                        </span>
                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                    <?php echo $email['endereco_email']; ?>
                    </a>
                </span>
                <?php endforeach; ?>

                <?php foreach ($enderecos as $endereco) : ?>
                    <span class="endereco mr-2 mb-2">
                        <span class="icon-wrapper">
                            <i class="fas fa-map-marker-alt mr-2"></i>
                        </span>
                        <span>
                        <?php echo $endereco['endereco']; ?>
                        <?php echo $endereco['bairro']; ?>
                        <?php echo $endereco['cidade_estado']; ?>
                            </span>
                    </span>
                <?php endforeach; ?>


            </div>


        </div><!-- row -->

    </div><!-- container -->
    <!-- Footer Links -->

    <!-- Copyright -->
    <?php get_template_part('/components/footer/cia-logo-footer'); ?>
    <!-- Copyright -->

</footer>
<div class="mobile-menu-overlay"></div>
<!-- Footer -->
<!--<div class="botao-lateral-wrapper">-->
<!--    <div class="botao-lateral">-->
<!--        <a title="Contato" href="#" rel="nofollow noreferrer noopener external" data-toggle="modal" data-target="#exampleModal">-->
<!--            <img src="--><?php //echo get_template_directory_uri() . '/assets/img/envelope-btn-lateral.png'; ?><!--"-->
<!--                 alt="--><?php //the_title() ?><!--" class="img-btn-lateral">-->
<!--        </a>-->
<!--    </div>-->
<!--</div>-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-primario fw-bold text-center w-100">Fale Conosco</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="wrapper-form-modal bg-secundario p-3 pt-5">
                <?php get_template_part('components/formularios/contact'); ?>
            </div>
        </div>
    </div>
</div>
<?php wp_footer() ?>

</body>

</html>