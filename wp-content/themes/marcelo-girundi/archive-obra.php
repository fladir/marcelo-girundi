<?php
/* Template Name: Nossas Obras */
get_header();
$produtos = get_field('produtos');
$args = array(
    'nopaging'               => false,
    'paged'                  => $paged,
    'post_type' => 'obra',
    'posts_per_page' => 6,
    'orderby' => 'post_date',
    'order' => 'DESC'
);
$WPQuery = new WP_Query($args);

?>
<!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

<section id="obras-realizadas">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center flex-column pb-5 mb-5">
                <h2 class="titulo">Obras <strong>Realizadas</strong></h2>
            </div>
        </div>
        <?php
        $i = 0;
        if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post();
            $class = $i % 2 ? 'flex-row-reverse' : '';
            ?>
            <div class="row <?php echo $class; ?> wow fadeIn">
                <div class="col-md-6 p-0">
                    <?php the_post_thumbnail('obras_home', array('class' => 'img-obras-realizadas', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>

                </div>
                <div class="col-md-6 p-0">
                    <div class="conteudo">
                        <h3 class="fw-bold mb-4"><?php the_title() ?></h3>
                        <?php the_excerpt(); ?>

                        <a href="<?php the_permalink() ?>" class="btn btn-secundario mt-2">Saiba Mais</a>
                    </div>

                </div>
            </div>
            <?php
            $i++;
        endwhile; endif;
        wp_reset_postdata(); ?>
        <div class="row my-5 pt-5">
            <div class="col-12 d-flex justify-content-center">
                <?php echo bootstrap_pagination($WPQuery); ?>
            </div>
        </div>
    </div>
</section>

<!-- Onde Estamos -->
<?php get_template_part('components/onde-estamos/onde-estamos'); ?>

<!-- Call to Action -->
<?php get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>
