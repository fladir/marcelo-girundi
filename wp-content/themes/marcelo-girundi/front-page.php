<?php get_header() ?>

<!-- Slide -->
<?php  get_template_part('components/slide-bootstrap/slide'); ?>

<!--O que é obesidade-->
<?php  get_template_part('components/index/o-que-e-obesidade'); ?>

    <!-- Conheça o Doutor -->
<?php  get_template_part('components/index/conheca-o-doutor'); ?>

<!-- Tratamentos Cirúrgicos -->
<?php  get_template_part('components/index/tratamentos-cirurgicos'); ?>

<!-- Depoimentos -->
<?php  get_template_part('components/index/depoimentos'); ?>

<!-- Dr. na Mídia -->
<?php  get_template_part('components/index/dr-na-midia'); ?>

<!-- Call to Action -->
<?php  #get_template_part('components/call-to-action/cta'); ?>

<?php get_footer() ?>