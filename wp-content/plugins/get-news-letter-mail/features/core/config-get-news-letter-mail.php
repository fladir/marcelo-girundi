<?php
    namespace CiaWebsites\GetNewsLetterMail;

    class NewsLetterConfig extends Feature{

        public function __construct(){
            $this->add_action('admin_menu', 'criar_menu');
            add_shortcode( 'get_news_letter_mail', $this->marshal('news_letter_form'));
        }

        public function criar_menu(){
            add_management_page('Get News Letter Mail', 'Get News Letter Mail', 'administrator', 'get-news-letter-mail', $this->marshal('pagina_get_news_letter_mail'));
        }

        public function pagina_get_news_letter_mail(){
            global $wpdb;
            $arr = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."get_news_letter_mail", ARRAY_A);
            $this->load_scripts();
            $template = $this->get_template();
            $options_default = get_option('opt-get-news-letter-mail');
            if($_POST){
                if(isset($_POST['delete'])){
                    $action = $_POST['action'];
                    if(isset($_POST['action'])){
                        foreach($action as $a){
                            $retorno = $this->delete_news_letter_mail($a);
                            if(!$retorno){
                                $template->set('mensagem', "<div class='error'><p> Erro ao excluir.</p></div>\n");
                                $err = true;
                                break;
                            }
                        }
                    }else{
                        $template->set('mensagem', "<div class='error'><p> Não foram marcados dados para exclusão.</p></div>\n");
                        $err = true;
                    }
                    if(!isset($err)){
                        $template->set('mensagem', "<div class='updated'><p> Excluido com sucesso.</p></div>\n");
                        $arr = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."get_news_letter_mail", ARRAY_A);
                    }
                }else{
                    if($_POST['archive_name']){
                        $archive_name = $_POST['archive_name'];
                    }else{
                        $archive_name = 'get-news-letter-mail';
                    }
                    if($_POST['archive_type'] == 'xml'){
                        $generate = $this->generate_xml($arr, $archive_name.'.xml');
                    }
                    if($_POST['archive_type'] == 'csv') {
                        $generate = $this->generate_csv($arr, $archive_name.'.csv');
                    }
                    if ($generate) {
                        $template->set('mensagem', "<div class='updated'><p> File have been generated successfully.</p></div>\n");
                    } else {
                        $template->set('mensagem', "<div class='error'><p> File generation error.</p></div>\n");
                    }
                    $data = array(
                        'url' => $generate,
                        'archive_type' => $_POST['archive_type'],
                        'archive_name' => $archive_name,
                    );
                    if($options_default){
                        update_option('opt-get-news-letter-mail',$data);
                    }else{
                        add_option('opt-get-news-letter-mail',$data);
                    }
                    $template->set('generate', true);
                }
            }
            if(isset($_GET['page']) && $_GET['page'] == 'get-news-letter-mail' && isset($_GET['delete']) && $this->search_array($_GET['delete'],$arr)){
                $retorno = $this->delete_news_letter_mail($_GET['delete']);
                if($retorno){
                    $template->set('mensagem', "<div class='updated'><p> Excluido com sucesso.</p></div>\n");
                }else{
                    $template->set('mensagem', "<div class='error'><p> Erro ao excluir.</p></div>\n");
                }
                $arr = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."get_news_letter_mail", ARRAY_A);
            }
            $template->set('all', $arr);
            $options_default = get_option('opt-get-news-letter-mail');
            $template->set('options_default', $options_default);
            echo $template->apply('form.php');
        }

        public function generate_xml($input_array, $file_name)
        {
            $XML = $this->xml();
            $this->array_to_xml($input_array , $XML);
            $xml_file = $XML->asXML(GET_NES_LETTER_MAIL_PLUGIN.'output/'.$file_name);
            if($xml_file){
                $resp =  PLUGIN_URL.'output/'.$file_name;
            }else{
                $resp = false;
            }
            return $resp;
        }

        function generate_csv($input_array, $file_name)
        {
            $fp = fopen(GET_NES_LETTER_MAIL_PLUGIN.'output/'.$file_name, 'w');
            fputcsv($fp, array('ID','Name','Email','Lista'));
            foreach ($input_array as $fields) {
                fputcsv($fp, $fields);
            }
            fclose($fp);

            return PLUGIN_URL.'output/'.$file_name;
        }

        private function load_scripts(){
            wp_register_script('get-news-letter-mail-script', plugins_url() . '/get-news-letter-mail/assets/js/scripts.js', array('jquery'), false, false);
            wp_register_style('get-news-letter-mail-style', plugins_url() . '/get-news-letter-mail/assets/css/styles.css', array(), '', 'all');
            wp_enqueue_script('get-news-letter-mail-script');
            wp_enqueue_style('get-news-letter-mail-style');
        }

        function news_letter_form($atts){
            $template = $this->get_template();
            $template->set('atts',$atts);
            if($_POST){
                $retorno = $this->register_news_letter_mail($_POST);
                $template->set('status',$retorno);
            }
            return $template->apply('news-letter-form.php');
        }

        private function register_news_letter_mail($data){
            global $wpdb;
            $query = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."get_news_letter_mail WHERE news_letter_mail = '".$data['news_letter_mail']."' ", ARRAY_A);

            /*if(!$data['news_letter_name']){
                return '<p class="email-error"> Você precisa digitar nome</p>';
            }
            if(!$data['news_letter_mail']){
                return '<p class="email-error"> Você precisa digitar um email</p>';
            }*/
            if(!$query){
                $table = $wpdb->prefix . 'get_news_letter_mail';
                $wpdb->insert($table, array('news_letter_name' => $data['news_letter_name'], 'news_letter_mail' => $data['news_letter_mail'], 'news_letter_list' => $data['news_letter_list']));
                $retorno = '<p class="email-susscess">Email cadastrado com sucesso</p>';
            }else{
                $retorno = '<p class="email-error">Email ja cadastrado</p>';
            }
            return $retorno;
        }

        private function delete_news_letter_mail($id){
            global $wpdb;
            $retorno = $wpdb->delete( $wpdb->prefix."get_news_letter_mail", array( 'news_letter_id' => $id ));
            return $retorno;
        }

    }
    add_action('get-news-letter-mail_init', array('CiaWebsites\GetNewsLetterMail\NewsLetterConfig', 'init'));