<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'marcelogirundi' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'ciaserver_user' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'ciaserver_mysql' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'hdWS}j>,Q4]=Bkt?fc:u1gyIdhdNXKX`QmZ- px[JwjqOS~#3;U95x N/E7k(L`h' );
define( 'SECURE_AUTH_KEY',  'O-e4*17>L,g3Lr&[IKOjXf<zKP*]]S&#R{R*-z:KKl$;1>*%kSHj%XeOaq6KW$kb' );
define( 'LOGGED_IN_KEY',    '0e%wUjxyFs)QNI%28#H%Z:v|=G{hy]<@;Vt4xjI6lTW?^/}1wzR/]OKJ06y!6# c' );
define( 'NONCE_KEY',        'TJ]fOa$*0&*|I9b!a$1#Fk-<z6#zdH+v6LY484bQ#1Ebz[%gY#@;b-^&zNP[iif#' );
define( 'AUTH_SALT',        '5%{S[G1e>Fp=Y(<QcOdOFVpEKB.]0PxH&[.ngo#0 =V#p3NCo$4TP$DV2M[5[*R}' );
define( 'SECURE_AUTH_SALT', '$KeC.Mfn7 *|A$][:0/b5>mFVlxL)lFp%.)fpBpSUN}LUo(X~f_cnVEq5~rhkT|n' );
define( 'LOGGED_IN_SALT',   'fZjJ/doH<t{jN!c7= f7Q7S@dtuKxQi#vO0W{Mv6#V`Pz1Ms==[SO=F0BqDLsL1X' );
define( 'NONCE_SALT',       'Pl6-0Wh(Mzuz[>6#oTZ`={+DPzGio_]LZpW#`B 3q^L_,=]4kbbno(`lX#2d`lAY' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

/** Sets up 'direct' method for wordpress, auto update without ftp */
define('FS_METHOD','direct');